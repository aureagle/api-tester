<?php

class Test {

	public $skip = false;
	public $passed = true;
	private $topic = "";

	/**
	@params testResult (boolean)
	@params testDesc (string) test description
	*/
	public function assertValid( $testResult, $testDesc, $valueReceived = 'undefined_value_assertvalid') {
		if( $testResult ) $this->passed = true;
		else $this->passed = false;
		
		if( $testResult ) echo "[passed] " . $this->topic . ": " . $testDesc . "\n";
		else {
			if( !$this->skip ) {
				if( $valueReceived != 'undefined_value_assertvalid' )
					echo "[failed] " . $this->topic . ": " . $testDesc . " (" . $valueReceived . ")". "\n";
				else 
					echo "[failed] " . $this->topic . ": " . $testDesc . "\n";
			} else {
				echo "[skipped] " . $this->topic . ": " . $testDesc . "\n";
			}
		}
	}

	public function skipped( $testDesc ) {
		echo "[skipped] " . $testDesc . "\n";
	}

	public function setTopic( $topic ) {
		$this->topic = $topic;
	}
}

$test = new Test();
