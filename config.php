<?php

class Config {

	protected  $env = 'staging'; // 'development' or 'production'
	public function __construct() {
		switch( $this->env ) {
		case 'development' :
			$this->site_url = 'http://localhost/tdp_crm';
			// $this->paramTest = 'http://localhost:8001/paramtest.php';
			break;
		case 'staging' :
			$this->site_url = 'https://staging3.rolustech.com:4436';
			break;
		case 'production':
			$this->site_url = '';
			// $this->paramTest = 'https://social.mytm.pk/paramtest.php';
			break;
		}

		$this->restUrl = $this->site_url . '/service/v4_1/rest.php';
	}

	// public $site_url = 'http://localhost:8888';
	public $site_url, $ajax, $msbridge, $social;

	public $a_login = 'admin';
	public $a_pass = '123';


	// takes an associative array of params
	// headers is not an associative array, it's an array of header lines instead of key/values
	public function curl( $apiUrl, $params = [], $headers = [], $method = 'post', $qstring = false ) {
		echo "doing curl...\n";
		$raw = false;
		$this->addToApi( [
			'URL' => $apiUrl,
			'params' => $params,
			'http_headers' => $headers,
			'http_method' => $method,
		]);

		$ch = curl_init();
		if( $qstring == false ) {
			$qstring = [];
			foreach( $params as $key => $value ) {
				$qstring[] = $key . "=" . $value;
			}
			$qstring = implode( "&", $qstring );			
		} else {
			$raw = true;
			$method = 'post';
		}
		if( $method == 'post' ) {
			curl_setopt( $ch, CURLOPT_URL, $apiUrl );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $qstring );
			curl_setopt( $ch, CURLOPT_POST, 1 );
			if( $raw ) curl_setopt( $ch, CURLOPT_HTTPHEADER, ['Content-Type' => 'text/plain'] );
		} else {
			curl_setopt( $ch, CURLOPT_URL, $apiUrl . '?' . $qstring );
		}
		if(!empty( $headers )){
			// error_log( "setting headers... " . json_encode( $headers ) );
			$e = ( function() use ( $headers ) {
				return $headers;
				// use the below code if headers is an associative array
				$r_header = [];
				foreach( $headers as $key => $value ) {
					$r_header[] = $key . ": " . $value;
				}
				return $r_header;
			})();

			curl_setopt( $ch, CURLOPT_HTTPHEADER, $e );
		}
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec( $ch );
		curl_close( $ch );
		$this->addToApi(['server_response' => $server_output]);
		return $server_output;
	}

	/*
	*/
	function curlFile( $apiUrl, $params = [], $headers = [], $method = 'post' ) {
		$ch = curl_init();
		$qstring = [];
		if( $method == 'post' ) {
			$qstring = $params;
			curl_setopt( $ch, CURLOPT_URL, $apiUrl );
		} else {
			foreach( $params as $key => $value ) {
				$qstring[] = $key . "=" . $value;
			}
			$qstring = implode( "&", $qstring );
			curl_setopt( $ch, CURLOPT_URL, $apiUrl . '?' . $qstring );
		}
		curl_setopt( $ch, CURLOPT_POST, 1 );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $qstring );
		if(!empty( $headers ))
			curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec( $ch );
		curl_close( $ch );
		return $server_output;
	}

	function set( $varname, $value ) {
		$this->$varname = $value;
	}

	function get( $varname ) {
		return $this->$varname;
	}

	private $apiData = [];
	function echoApi() {
		foreach( $this->apiData as $key => $value ) {
			if( is_array( $value )) $value = json_encode( $value );
			echo "{$key}: {$value}\n\n";
		}
	}

	function addToApi( $kvp ) {
		foreach( $kvp as $key => $value ) {
			$this->apiData[ $key ] = $value;
		}
	}
}

$config = new Config;

